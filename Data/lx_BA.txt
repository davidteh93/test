{NUMERIC:5:5}	{1}	PostalCodePrimary	250
{NUMERIC:4}	0{1}	PostalCodePrimary	150
{NUMERIC:2} {NUMERIC:3:3}	{1}{2}	PostalCodePrimary	250
{ASCII:2} {NUMERIC:5:5}	{2}	PostalCodePrimary	250
BIH {NUMERIC:5:5}	{1}	PostalCodePrimary	250
BIH {NUMERIC:4}	0{1}	PostalCodePrimary	200


BIBLIOTECA	Bib.	SubBuildingType	100
KLINIKA	Klin.	SubBuildingType	100


Biblioteca	Bib.	BuildingType	100
centar	Centar	BuildingType	100
Klinika	Klin.	BuildingType	100
Stadion	Stadion	BuildingType	100
Terminal	Terminal	BuildingType	200


(b b|bb|vv)	bb	PremiseNumber	255
{NUMERIC:1:4}/{ASCII:1:3}	{1}/{2}	PremiseNumber	100


Alipasino	Alipasino	ThoroughfareName	100
BOSANSKA	Bosanska	ThoroughfareName	100
Cim	Cim	ThoroughfareName	100
DONJE PUTICEVO	Donje Puticevo	ThoroughfareName	100
Halilovići	Halilovići	ThoroughfareName	100
I F Jukica	I F Jukica	ThoroughfareName	100
I Krajiskog proleterskog bataljona	I Krajiskog Proleterskog Bataljona	ThoroughfareName	100
K Stepinca	K Stepinca	ThoroughfareName	100
M ef Pandže	M ef Pandže	ThoroughfareName	100


{NUMERIC:1:3} brigade	{1} Brigade	ThoroughfareName	100
{NUMERIC:1:3} korpusa	{1} Korpusa	ThoroughfareName	100
{NUMERIC:1:3} Slavne Brdske Brigade	{1} Slavne Brdske Brigade	ThoroughfareName	100
{NUMERIC:1:3} VITEŠKE BRIGADE	{1} Viteške Brigade	ThoroughfareName	100


{NUMERIC:1:2} APRILA	{1} Aprila	ThoroughfareName	100
{NUMERIC:1:2} Maj	{1} Maj	ThoroughfareName	100
{NUMERIC:1:2} OKTOBAR	{1} Oktobar	ThoroughfareName	100
{NUMERIC:1:2} NOVEMBAR	{1} Novembar	ThoroughfareName	100
{NUMERIC:1:2} DECEMBAR	{1} Decembar	ThoroughfareName	100


blok	blok	ThoroughfareType	100
(la|lane)	La	ThoroughfareType	255
ul	ulica	ThoroughfareType	100
blvd	Blvd	ThoroughfareType	100
cesta	cesta	ThoroughfareType	100
trg	trg	ThoroughfareType	100


(po box|pob)	P.F.	PostBoxType	100


Tržni centar Sjenjak	Tržni centar Sjenjak	DependentLocality	100


(Banjaluka|Banja Luka) {NUMERIC:0:4}	Banjaluka	Locality	255
// won't verify even though valid in GEON. Entry needed just to get valid city into city field
Bijeljina {NUMERIC:0:4}	Bijeljina	Locality	255
//BREZIK BRCKO DISTRICT {NUMERIC:0:4}	Brcko	Locality	100
BUSOVA A {NUMERIC:0:4}	Busovača	Locality	100
CORALICI {NUMERIC:0:4}	Ćoralići	Locality	100
Doboj {NUMERIC:0:4}	Doboj	Locality	100
GRA?ANICA {NUMERIC:0:4}	Gračanica	Locality	100
ILID A {NUMERIC:0:4}	Ilidža	Locality	100
LJUBU KI {NUMERIC:0:4}	Ljubuški	Locality	100
OSTROŽAC KOD CAZINA {NUMERIC:0:4}	Ostrožac	Locality	100
// Sarajevo and Banja Luka recodes cause some valid V22 records to become A or even U. With or without, the city is mapped to Locality but something changes, which make it drop the score
// On the other hand, some records go from A/U or unparsed to V22. This makes it impossible to have the best of both and, hence, the target is unattainable
(SAREJEVO|SARAJAVO|Sarajevo) {NUMERIC:0:4}	Sarajevo	Locality	255
Novo Sarajevo {NUMERIC:0:4}	Novo Sarajevo	Locality	100
pale {NUMERIC:1:4}	Pale	Locality	100
// This city goes to building unless defined here with a 255 score, whereupon it validates as Locality with a V22!?
Prijedor {NUMERIC:0:4}	Prijedor	Locality	255
sm	Sanski Most	Locality	100
STRURLIC {NUMERIC:0:4}	Sturlic	Locality	100
TE ANJ {NUMERIC:0:4}	Tešanj	Locality	100
TRAVNIK {NUMERIC:0:4}	Travnik	Locality	255
(Tuzle|Tuzla) {NUMERIC:0:4}	Tuzla	Locality	100
VELIKA KLADUSA	Velika Kladuša	Locality	255


Kanton Sarajevo	Sarajevski Kanton	AdministrativeArea	100


{NUMERIC:6:12}	{1}	Telephone	100


Bosnia and Herzegovina		Junk	1
FBIH		Junk	100

// SB rule
// try removing various pafs

(Brčko Distrikt)	Brčko Distrikt	SuperAdministrativeArea	200
(Federacija Bosne i Hercegovine)	Federacija Bosne i Hercegovine	SuperAdministrativeArea	200
(Republika Srpska)	Republika Srpska	SuperAdministrativeArea	200
(Banja Luka)	Banja Luka	AdministrativeArea	210
(Bosansko-Podrinjski Kanton)	Bosansko-Podrinjski Kanton	AdministrativeArea	210
(Brčko Distrikt)	Brčko Distrikt	AdministrativeArea	210
(Doboj)	Doboj	AdministrativeArea	210
(Foča)	Foča	AdministrativeArea	210
(Hercegovačko-Neretvanski Kanton)	Hercegovačko-Neretvanski Kanton	AdministrativeArea	210
(Livanjski Kanton)	Livanjski Kanton	AdministrativeArea	210
(Posavski Kanton)	Posavski Kanton	AdministrativeArea	210
(Sarajevo-Romanija)	Sarajevo-Romanija	AdministrativeArea	210
(Sarajevski Kanton)	Sarajevski Kanton	AdministrativeArea	210
(Srednjebosanski Kanton)	Srednjebosanski Kanton	AdministrativeArea	210
(Tuzlanski Kanton)	Tuzlanski Kanton	AdministrativeArea	210
(Unsko-Sanski Kanton)	Unsko-Sanski Kanton	AdministrativeArea	210
(Zeničko-Dobojski Kanton)	Zeničko-Dobojski Kanton	AdministrativeArea	210
(Bagnaluca|Bania Louka|Banja Luka|Banya-Luka|Lukácsbánya|Μπανια Λουκα|Баня-Лука)	Banja Luka	Locality	220
(Bihac)	Bihac	Locality	220
(Bichak|Bihac|Bihać|Bihač|Bihács|Bikhach|Μπιχακ|Бихач)	Bihać	Locality	220
(Bosanska Gradiška-Gradiška)	Bosanska Gradiška-Gradiška	Locality	220
(Bosanski Šamac-Šamac)	Bosanski Šamac-Šamac	Locality	220
(Bosansko-Podrinjski Kanton)	Bosansko-Podrinjski Kanton	Locality	220
(Brčko Distrikt)	Brčko Distrikt	Locality	220
(Bugojno)	Bugojno	Locality	220
(Derventa)	Derventa	Locality	220
(Doboj)	Doboj	Locality	220
(Foča)	Foča	Locality	220
(Foča-Srbinje)	Foča-Srbinje	Locality	220
(Goražde)	Goražde	Locality	220
(Gradačac)	Gradačac	Locality	220
(Hercegovačko-Neretvanski Kanton)	Hercegovačko-Neretvanski Kanton	Locality	220
(Jajce)	Jajce	Locality	220
(Ključ)	Ključ	Locality	220
(Konjic)	Konjic	Locality	220
(Livanjski Kanton)	Livanjski Kanton	Locality	220
(Modrica)	Modrica	Locality	220
(Mostar|Μοσταρ|Мостар)	Mostar	Locality	220
(Mrkonjič Grad)	Mrkonjič Grad	Locality	220
(Novi Grad)	Novi Grad	Locality	220
(Novo Sarajevo)	Novo Sarajevo	Locality	220
(Posavski Kanton)	Posavski Kanton	Locality	220
(Sarajevo-Romanija)	Sarajevo-Romanija	Locality	220
(Sarajevski Kanton)	Sarajevski Kanton	Locality	220
(Srednjebosanski Kanton)	Srednjebosanski Kanton	Locality	220
(Travnik)	Travnik	Locality	220
(Touzla|Tuzla|Τουζλα|Тузла)	Tuzla	Locality	220
(Tuzlanski Kanton)	Tuzlanski Kanton	Locality	220
(Unsko-Sanski Kanton)	Unsko-Sanski Kanton	Locality	220
(Zenica|Zenika|Zenitsa|Ζενικα|Зеница)	Zenica	Locality	220
(Zeničko-Dobojski Kanton)	Zeničko-Dobojski Kanton	Locality	220
(Bihac)	Bihac	DependentLocality	205
(Bichak|Bihac|Bihać|Bihács|Bikhach|Μπιχακ|Бихач)	Bihać	DependentLocality	205
(Ilidža)	Ilidža	DependentLocality	205
(Saraevo|Saragievo|Sarajevo|Sarajewo|Szaravejó|Σαραγιεβο|Сараево)	Sarajevo	DependentLocality	205
