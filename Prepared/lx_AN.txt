(b v|bv|BESLOTEN VENNOOTSCHAP)	BV	OrganizationType	100
(MIJ|my|maatschappij)	Maatschappij	OrganizationType	100
(n v|NV|naamloze vennootschap)	NV	OrganizationType	100
STICHTING	Stichting	OrganizationType	100
Vereniging	Vereniging	OrganizationType	100
(VENNOOTSCHAP ONDER FIRMA|V O F|VOF)	VOF	OrganizationType	100


box	Bus	SubBuildingType	100
KAMER	Kamer	SubBuildingType	100
Kant	Kant	SubBuildingType	100
(KANT|KANTOOR)	Kantoor	SubBuildingType	100
Poort	Poort	SubBuildingType	100
(Verdieping|VERD)	Verdieping	SubBuildingType	100


FAC DER	Fac. der	BuildingType	100
gasthuis	Gasthuis	BuildingType	100
(LANDHUIS|Landhius)	Landhuis	BuildingType	100
Poort	Poort	BuildingType	100
(Zh|ziekenhuis)	Ziekenhuis	BuildingType	100


(zn|z n)	Z/N	PremiseNumber	150


(BN|Baan|BANN)	Baan	ThoroughfareType	100
b vd	Blvd	ThoroughfareType	100
DREEF	Dreef	ThoroughfareType	100
(DWSTR|dwarsstraat)	Dwarsstraat	ThoroughfareType	100
GRACHT	gracht	ThoroughfareType	100
Kaya	Kaya	ThoroughfareLeadingType	100
(laan|LN)	Laan	ThoroughfareType	100
pad	Pad	ThoroughfareType	100
PL	Pl	ThoroughfareType	100
Plaza	Plaza	ThoroughfareType	100
(plein|PLN)	Plein	ThoroughfareType	100
(sngl|Singel)	Singel	ThoroughfareType	100
SPOOR	Spoor	ThoroughfareType	100
(steeg|Sg|stg)	Steeg	ThoroughfareType	100
(SWG|Stwg|STEENWEG)	Steenweg	ThoroughfareType	100
(str|straat|STRATT|STAAT|SRAAT)	Straat	ThoroughfareType	100
(straatweg|Strwg)	Straatweg	ThoroughfareType	100


Kaya W F G Mensing	Kaya W F G Mensing	ThoroughfareName	100


box	Bus	PostBoxType	100
(PO BOX|p o box|postbus|pobox|POB|PB)	Postbus	PostBoxType	100


Pondfill	Pondfill	DependentLocality	205


(BELVEDER|Belvédère)	Belvédère	Locality	220
(Colebay|Cole Bay)	Cole Bay	Locality	220
(PHILPSBURG|PHILLIPSBURG|Philuipsburg|Filipsburg|Great Bay|Philipsburg|Филипсбург)	Philipsburg	Locality	220
(PT|Point|Pointe) (Blanche)	Pointe Blanche	Locality	220
(Salinja Salinja|Salinja)	Salinja	Locality	220
(Sharloo|Schaarlo|Scharloo)	Scharloo	Locality	220
(WILLEMSTADT|WILLENSTAD|Williamsted|Willemsta|willemsatd|Wilemstad|Willemstad)	Willemstad	Locality	220
(WILLEMSTADT|WILLENSTAD|Williamsted|Willemsta|willemsatd|Wilemstad|Willemstad) (WILLEMSTADT|WILLENSTAD|Williamsted|Willemsta|willemsatd|Wilemstad|Willemstad)	Willemstad	Locality	220
(WILLEMSTADT|WILLENSTAD|Williamsted|Willemsta|willemsatd|Wilemstad|Willemstad) (CURACA|curacao) (WILLEMSTADT|WILLENSTAD|Williamsted|Willemsta|willemsatd|Wilemstad|Willemstad)	Willemstad	Locality	220


(BONAIR|Bonaire)	Bonaire	AdministrativeArea	210
Insel (BONAIR|Bonaire)	Bonaire	AdministrativeArea	210
(CURACA|Curaco|Curacao)	Curaçao	AdministrativeArea	210
insel (CURACA|Curaco|Curacao)	Curaçao	AdministrativeArea	210
(Saint|Sint|Zint|St) (Maarter|Maarten)	St. Maarten	AdministrativeArea	210
insel (Saint|Sint|Zint|St) (Maarter|Maarten)	St. Maarten	AdministrativeArea	210


(AN|ANT|Netherlands Antilles Netherlands Antilles|neth Antilles|Netherlands Antilles)	Netherlands Antilles	Country	10
(neth|Ned) (Ant|Antilles)	Netherlands Antilles	Country	10


FLORIDA		LocalityJunk	10
NOORD BRABANT		LocalityJunk	10


#import ../AutoLex/lx_rd_AN_geo.txt
