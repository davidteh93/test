#import lx_AA.txt:-SubBuildingNumber

(AZ|AZE) {NUMERIC:4:4}	AZ {1}	PostalCodePrimary	250
(AZ|AZE) {NUMERIC:3:3}	AZ 0{1}	PostalCodePrimary	250
{NUMERIC:4:4}	AZ {1}	PostalCodePrimary	250
{NUMERIC:3:3}	AZ 0{1}	PostalCodePrimary	100


Apt	Ap.	SubBuildingType	100
Menzil	Menzil	SubBuildingExtra	100

// SubBuildingNumber
{NUMERIC:1:4}	{1}	SubBuildingNumber	85
{NUMERIC:1:4}{ASCII:1}	{1}{2}	SubBuildingNumber	85
{ASCII:1}{NUMERIC:1:4}	{1}{2}	SubBuildingNumber	85
{NUMERIC:1:4}-{NUMERIC:1:4}	{1}-{2}	SubBuildingNumber	150
{NUMERIC:1:4}/{NUMERIC:1:4}	{1}/{2}	SubBuildingNumber	150
{NUMERIC:1:4}&{NUMERIC:1:4}	{1} & {2}	SubBuildingNumber	200
{ASCII:1}-{ASCII:1}	{1}-{2}	SubBuildingNumber	150
{ASCII:1}-{NUMERIC:1:4}	{1}-{2}	SubBuildingNumber	150
{ASCII:1}&{ASCII:1}	{1} & {2}	SubBuildingNumber	150
(SIN NUMERO|SIN NUMERO 0|SEM NUMERO|SEM NUMERO 0|S N|SN|S N 0|SN 0)	S/N	SubBuildingNumber	190


cntr of	Cent. of	BuildingType	100
units	Units	BuildingType	100
cu mehelle ev	cu mehelle ev	BuildingType	100

{NUMERIC:1:3} кв	{1}  кв	Premise	100

PR	pr.	ThoroughfareType	100
(Prosp|Prospekiti|Prospecti)	Prospekti	ThoroughfareType	100
SHOSSE	shosse	ThoroughfareType	100
ul	ul.	ThoroughfareType	100


locked bag	Locked Bag	PostBoxType	100

(Səbayel)	Səbayel	DependentLocality	215

Baku	Bakı	Locality	220

Baku	Bakı	AdministrativeArea	210

#import ../AutoLex/lx_rd_AZ_vfy.txt